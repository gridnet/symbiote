var mysql = require('mysql');
var MysqlJson = require('mysql-json');
var express = require('express');
var symbiote = express();
var zeroFill = require('zero-fill')

var con = mysql.createConnection({
  host: "gridnet.gr",
  user: "sensors_fiesta",
  password: "*******",
  database: "gridnet_gr_fiesta"
});
      
var sensor_id = 1;
var node_gps_data = [
    {
        "longitude": "39.355547",
        "latitude": "22.925942",
        "altitude": "1"
    },
    {
        "longitude": "39.361034",
        "latitude": "22.949900",
        "altitude": "1"
    },
    {
        "longitude": "39.358156",
        "latitude": "22.943387",
        "altitude": "1"
    },
    {
        "longitude": "39.386802",
        "latitude": "22.932674",
        "altitude": "1"
    },
    {
        "longitude": "39.388193",
        "latitude": "22.985764",
        "altitude": "1"
    },
    {
        "longitude": "39.376751",
        "latitude": "22.928367",
        "altitude": "1"
    },
    {
        "longitude": "39.370464",
        "latitude": "22.915406",
        "altitude": "1"
    },
    {
        "longitude": "39.370163",
        "latitude": "22.948385",
        "altitude": "1"
    },
    {
        "longitude": "39.357433",
        "latitude": "22.960069",
        "altitude": "1"
    },
    {
        "longitude": "0.0",
        "latitude": "0.0",
        "altitude": "1"
    },
    {
        "longitude": "39.366311", 
        "latitude": "22.939240",
        "altitude": "1"
    }
];
var gpsAirArray = [ ["39.355547", "22.925942"], ["39.361034", "22.949900"], ["39.366311", "22.939240"],  ["39.358156", "22.943387"], ["39.388193", "22.985764"],["39.376751","22.928367"],["39.370464", "22.915406"],["39.370163","22.948385"],["39.357433", "22.960069"],["0.0","0.0"],["39.386802","22.932674"]]; //GW GPS "39.366056","22.923889"
 //create 3 arrays      
var db_names = ["atemp", "ahumid", "co","no2","nh3","so2","o3","dust_pm10","bar_pressure"];
var symbiote_names = ["temperature", "humidity", "carbonMonoxideConcentration","nitrogenDioxideConcentration","ammoniaConcentration","sulphurDioxideConcentration","ozoneConcentration","ParticulateMatter_LessThan_10um_aerosol_Concentration","atmosphericPressure"];
var units = ["°C", "%", "ppm","ppm","ppm","ppm","ppm","μg/m^3","kPa"];




// var obs_data_model = {
//     "value": "",
//     "obsProperty": {
//       "@c": ".Property",
//       "name": "",
//       "description": ""
//     },
//     "uom": {
//       "@c": "UnitOfMeasurment",
//       "symbol": "",
//       "name": "",
//       "description": ""
//     }
// }
con.connect(function(err) {
  if (err) throw err;
  
});

function get_data(id,callback){
    con.query("SELECT * FROM sensor_fiesta_air_live WHERE sensor_id=" + id, function (err, result, fields) {
        if (err) throw err;
        console.log(result[0].sensor_id);
        var symbiote_data_model ={
            "resourceId": "",
            "location": {},
            "resultTime": "1970-1-1T02:00:12",
            "samplingTime": "1970-1-1T02:00:12",
            "obsValues":[]
        }


        var padded_id = zeroFill(2,(result[0].sensor_id));
        symbiote_data_model.resourceId = "11:22:33:44:55:" + padded_id;
        symbiote_data_model.location = node_gps_data[result[0].sensor_id - 1];
        symbiote_data_model.resultTime = result[0].timestamp;
        symbiote_data_model.samplingTime = result[0].timestamp;

        for(key in result[0])
        {
            if(result[0].hasOwnProperty(key))
            {     
                var index = db_names.indexOf(key);
                if (index >= 0)
                {
                    
                    if((result[0])[key]!=null){
                        var obs_data_model = {
                            "value": "",
                            "obsProperty": {
                            "@c": ".Property",
                            "name": "",
                            "description": ""
                            },
                            "uom": {
                            "@c": "UnitOfMeasurment",
                            "symbol": "",
                            "name": "",
                            "description": ""
                            }
                        }
                        // console.log((result[0])[key]);
                        obs_data_model.value = (result[0])[key];
                        obs_data_model.obsProperty.name = symbiote_names[index];
                        obs_data_model.uom.symbol = units[index];
                        obs_data_model.uom.name = units[index];

                        if (result[0].sensor_id == "11")
                        {
                            console.log(result[0])
                        }
                        // console.log(obs_data_model);
                        // if (symbiote_names[index] == "ozoneConcentration" && result[0].sensor_id == "11")
                        // {
                        //     //do nothing
                        // }
                        // else
                        // {
                            symbiote_data_model.obsValues.push(obs_data_model);
                        // }
                        
                    }
                }
                // console.log(symbiote_names[index]);
            }
        }
        var result_json = JSON.stringify(symbiote_data_model);
        // console.log(result_json);
        
        return callback(symbiote_data_model);

    });
}


symbiote.post('/json/:id', function (req, res) {
    res.type('json');
    var json_object = [];

    if(req.params.id > 0 && req.params.id < 20)
    {
        get_data(req.params.id,function(result){
            console.log(JSON.stringify(result));      
            res.write(JSON.stringify(result));
            res.end();
        });
    }
    // console.log(json_object);
    // console.log(JSON.stringify(json_object));
    // res.write("{poutsa:1}");
    // res.write(JSON.stringify(json_object));
    
})

symbiote.listen(3000, function () {
    console.log('Symbiote RAP Agent listening on port 3000!');
})

